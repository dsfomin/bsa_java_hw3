export function fight(firstFighter, secondFighter) {
  var firstFighterHealth = firstFighter.health;
  var secondFighterHealth = secondFighter.health;

  while (firstFighterHealth > 0 && secondFighterHealth > 0) {
    secondFighterHealth -= getDamage(firstFighter, secondFighter);
    firstFighterHealth -= getDamage(secondFighter, firstFighter);
  }

  return firstFighterHealth > 0 ? firstFighter : secondFighter;
}

export function getDamage(attacker, enemy) {
  let attakerHitPower = getHitPower(attacker);
  let enemyBlockPower = getBlockPower(enemy);
  let damage = attakerHitPower - enemyBlockPower;
  return damage >= 0 ? damage : 0;
}

export function getHitPower(fighter) {
  let probability = Math.random() + 1;
  let hitPower = probability * fighter.attack;
  return hitPower;
}

export function getBlockPower(fighter) {
  let probability = Math.random() + 1;
  let blockPower = probability * fighter.defense;
  return blockPower;
}
