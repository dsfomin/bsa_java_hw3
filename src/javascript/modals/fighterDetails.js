import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { createImage } from '../fighterView';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, health, attack, defense, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
  const imageElement = createImage(source);

  nameElement.innerHTML = `<p> Name: ${name} </p>`;
  healthElement.innerHTML = `<p> Health: ${health} </p>`;
  attackElement.innerHTML = `<p> Attack: ${attack} </p>`;
  defenseElement.innerHTML = `<p> Defense: ${defense} </p>`;

  fighterDetails.append(nameElement, healthElement, attackElement, defenseElement, imageElement);

  return fighterDetails;
}
