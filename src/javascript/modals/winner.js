import { showModal } from './modal';
import { createElement } from '../helpers/domHelper';
import { createImage } from '../fighterView';

export  function showWinnerModal(fighter) {
  const element = createElement({ tagName: 'div', className: 'modal-body' });
  element.style.textAlign = "center";
  const imageElement = createImage(fighter.source);
  element.innerHTML = `<p> Winner: ${fighter.name} </p>`;
  element.append(imageElement);
  showModal({ title: `GAME OVER`, bodyElement: element });
}